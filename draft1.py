#KASHISH WAS HERE

#importing required libraries for the python environment
import sys
import csv
import math
import os.path
import time
from time import sleep

#importing required libraries for syringe pump automation
import serial 
import keyboard


comportnumber=raw_input("Enter comport number (eg.COM3) that the syringe pump is connected to: ")
comport = serial.Serial(comportnumber,9600, timeout = 1)

#Trinamic set up protocol.

#Step 1: Enabling limit switch command; direction can be either left or right
#Here I coded for the limit switch to be left
comport.write('\x01\x05\x0D\x00\x00\x00\x00\x00\x13')
print(comport.readlines())


#Step 2: Move Motor Command; direction depends on the direction of the limit switch. 
#Here the Motor is moving left
comport.write('\x01\x02\x00\x00\x00\x00\x03\x20\x26')
print(comport.readlines())
#Motor will stop at the once it hits the limit switch


#Step 3: Send the 'Stop Motor' command
comport.write('\x01\x03\x00\x00\x00\x00\x00\x00\x04')
print(comport.readlines())



#Step 4: Disable the left limit switch
comport.write('\x01\x05\x0D\x00\x00\x00\x00\x01\x14')
print(comport.readlines())


#Step 5: Set the position to 0
comport.write('\x01\x05\x01\x00\x00\x00\x00\x00\x07')
print(comport.readlines())



#KASHISH WAS HERE TOO
